# Pixel Art Transformation
 
Image Processing - scc0251.
 
Final Project - 2020
 
Authors:
* Eleazar Fernando Braga
* Lucas Faria Hermeto

Video: https://drive.google.com/file/d/100rvXNvR98y0Tklk8_gIwb3JaA10Ndgz/view?usp=sharing
 
## Abstract
The main goal of this project's to apply transformation on imagens and convert them to pixel art and tranform pixel art to tiny images.
So the project is based on 2 main goals:
- Use PCA to apply transformations at images and generate pixel art from it.
- Use better methods of interpolation to convert pixel art to tiny image.
 
The main use of this applications will be generating easy pixel arts to games designers.
 
---
## Usage
### Input
To run the program, the program will ask to input: the image and the out image size
 
The images that will be used as input must be:
- Raster image
- Uniform Background
 
The image size must be 3 numbers only:
- 32
- 64
- 128
 
### Output
The result image will be an pixel art with the size and proportion previously defined by the input image. So:
- input size: 32 and input image size: (x,x) -> output : 32x32
 
But, if the input image has the proportion (250, 361) the output will maintain the proportion by transforming the small size to input size and the other will be multiplied by the proportion:
- input size: 32 and proportion: 250/32 = 12,8% -> output: 32x(361 * 12.8%) = 32x46
 
---
## Example Images
To generate the output images and to test if our program is good we used a payed program called [Aseprite](https://www.aseprite.org/).
### Example 1 
![README/example1.jpg](README/example1.jpg)
![README/example1.out.jpg](README/example1.out.jpg)
 
### Example 2
![README/example2.jpg](README/example2.jpg)
![README/example2.out.jpg](README/example2.out.jpg)
 
### Example 3
![README/example3.jpg](README/example3.jpg)
![README/example3.out.jpg](README/example3.out.jpg)
 
### Example 4
![README/example4.jpg](README/example4.jpg)
![README/example4.out.jpg](README/example4.out.jpg)

---
## Goals
We choose 4 types of images, from simple to complex images to test if our code is getting the best results.

To get the better results we will start implementing an resizer program combined with an transformation that will get the best result of pixels concatenation.